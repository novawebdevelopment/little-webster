import asyncio
import irc3
import os

from irc3.plugins.logger import file_handler
from irc3.plugins.command import command
from irc3.plugins.cron import cron
from time import gmtime, strftime


class handler(file_handler):
    formatters = {
        "privmsg": "{date:%H:%M:%S} <{mask.nick}> {data}",
        "join": "{date:%H:%M:%S} *** {mask.nick} has joined {channel}",
        "part": "{date:%H:%M:%S} *** {mask.nick} has left {channel} ({data})",
        "quit": "{date:%H:%M:%S} *** {mask.nick} has quit ({data})",
        "notice": "{data}",
    }


class AgendaHandler:
    agenda_file = "/srv/little-webster/agenda.dat"

    def __init__(self):
        if not os.path.exists(self.agenda_file):
            open(self.agenda_file, "w").close()

    def get_items(self):
        with open(self.agenda_file, "r") as f:
            for line in f:
                yield line.strip()

    def add_item(self, item):
        if not item in self.get_items():
            with open(self.agenda_file, "a") as f:
                f.write(item + "\n")
                return True
        return False

    def remove_item(self, item):
        item_removed = False
        with open(self.agenda_file, "r+") as f:
            lines = f.readlines()
            f.seek(0)
            for line in lines:
                if line.strip() != item:
                    f.write(line)
                else:
                    item_removed = True
            f.truncate()
        return item_removed

    def clear_items(self):
        open(self.agenda_file, "w").close()


@irc3.plugin
class MeetingManager:
    def __init__(self, bot):
        self.bot = bot
        self.agenda_handler = AgendaHandler()

    @command
    async def agenda(self, mask, target, args):
        """
        List the agenda items.
        %%agenda
        """
        items = list(self.agenda_handler.get_items())
        if items:
            self.bot.privmsg(target, f"This is the agenda for the next meeting:", nowait=True)
            for item in items:
                await asyncio.sleep(0.25)
                self.bot.privmsg(target, item, nowait=True)
        else:
            self.bot.privmsg(target, "Warning: There are no items on the agenda!", nowait=True)

    @command
    def add(self, mask, target, args):
        """
        Add an item to the agenda.
        %%add [<text>...]
        """
        if args["<text>"]:
            text = " ".join(args["<text>"]).strip()
            if self.agenda_handler.add_item(f"{text} (added by {mask.nick})"):
                self.bot.privmsg(target, f'Success: "{text}" has been added to the agenda.', nowait=True)
            else:
                self.bot.privmsg(target, f'Error: "{text}" is already on the agenda!', nowait=True)
        else:
            self.bot.privmsg(target, "Error: Cannot add an empty item to the agenda!", nowait=True)

    @command
    def remove(self, mask, target, args):
        """
        Remove an item from the agenda. (Must be the creator of the item)
        %%remove [<text>...]
        """
        if args["<text>"]:
            text = " ".join(args["<text>"]).strip()
            if self.agenda_handler.remove_item(f"{text} (added by {mask.nick})"):
                self.bot.privmsg(target, f'Success: "{text}" has been removed from the agenda.', nowait=True)
            else:
                self.bot.privmsg(
                    target, f'Error: "{text}" is not on the agenda or was added by someone else!', nowait=True
                )
        else:
            self.bot.privmsg(target, "Error: Cannot remove an empty item from the agenda!", nowait=True)

    @cron("0 14 * * 6")
    async def run_meeting(self, target=None):
        if target is None:
            target = self.bot.config["autojoins"][0]

        self.bot.privmsg(
            target, " : ".join(user for user in self.bot.channels[target] if user != self.bot.nick), nowait=True
        )
        self.bot.privmsg(target, "Tick Tock!", nowait=True)
        await asyncio.sleep(1)
        self.bot.privmsg(
            target,
            f"It's {strftime('%H:%M', gmtime())} UTC o'clock and NOVA Web Development's meeting is starting.",
            nowait=True,
        )
        await asyncio.sleep(1)

        items = list(self.agenda_handler.get_items())
        if items:
            self.bot.privmsg(target, f"This is the agenda for today:", nowait=True)
            for item in items:
                await asyncio.sleep(0.25)
                self.bot.privmsg(target, item, nowait=True)
        else:
            self.bot.privmsg(target, "Uh-oh! That is embarassing...", nowait=True)
            self.bot.privmsg(target, "Unfortunately, there are no items on the agenda!", nowait=True)

        self.bot.privmsg(target, "Have a nice day, Websters!", nowait=True)

    @cron("30 14 * * 6")
    def clear_agenda_items(self, target=None):
        self.agenda_handler.clear_items()

    @command(show_in_help_list=False)
    async def forcerun(self, mask, target, args):
        """Force cron job playback
        %%forcerun
        """
        await self.run_meeting()
